#include <customutils.h>

namespace customutils
{
	std::vector<int> getNInput(int n)
	{
		while (true)
		{
			try
			{
				std::vector<int> nums;
				std::string str;
				size_t idx;

				std::getline(std::cin, str); // get input

				// parse input
				for (int i = 0; i < n; i++)
				{
					nums.push_back(std::stoi(str, &idx));
					str = str.substr(idx);
				}
				return nums;
			}
			catch (std::exception e)
			{
				std::cout << "Wrong inputs !" << std::endl;
			}
		} // END while true

	} // END function getNInput

} // END namespace customutils

