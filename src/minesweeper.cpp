#include "minesweeper.h"

/** ------------ */
/** CONSTRUCTORS */
/** ------------ */

Minesweeper::Minesweeper()
{
	board = nullptr;
	size = 0;
	cd_win = 0;
}

Minesweeper::~Minesweeper()
{
	delete[] board;
	board = nullptr;
}

/** --------- */
/** FUNCTIONS */
/** --------- */

void Minesweeper::setup()
{
	std::cout << "Please enter the size of the grid : ";
	size = customutils::getNInput(1)[0];
	board = new Cell*[size * size];

	srand(time(0));
	// init cells
	for (int y = 0; y < size; y++)
	{
		for (int x = 0; x < size; x++)
		{
			bool trapped = !(rand() % 10);
			if (trapped) cd_win++;
			board[index(x ,y)] = new Cell(trapped);
		}
	} // END for y
	cd_win = size * size - cd_win;

	// update neighbours
	for (int y = 0; y < size; y++)
	{
		for (int x = 0; x < size; x++)
			updateNeighbours(x, y);
	} // END for y

}

void Minesweeper::run()
{
	bool win = false;
	std::vector<int> coord;
	while (true)
	{
		display();
		do
		{
			std::cout << "Enter coordinates (y, x) : ";
			coord = customutils::getNInput(2);
		}
		while (coord[0] < 0 || coord[0] >= size || coord[1] < 0 || coord[1] >= size);

		reveal(coord[1], coord[0]);

		if (board[index(coord[1], coord[0])]->isTrapped())
			break;

		if (!cd_win)
		{
			win = true;
			break;
		}
	}

	for (int y = 0; y < size; y++)
	{
		for (int x = 0; x < size; x++)
		{
			board[index(x, y)]->setRevealed();
		}
	} // END for y
	display();

	if (win) std::cout << "You win !";
	else std::cout << "You loose !";


}

void Minesweeper::updateNeighbours(int x, int y)
{
	int xStart = (x == 0) ? x : x - 1;
	int xEnd = (x == size - 1) ? x : x + 1;

	int yStart = (y == 0) ? y : y - 1;
	int yEnd = (y == size - 1) ? y : y + 1;

	int neighbours = 0;

	for (int i = yStart; i <= yEnd; i++)
	{
		for (int j = xStart; j <= xEnd; j++)
		{
			if ((i != y || j != x) && board[index(j, i)]->isTrapped())
				neighbours++;
		}
	}
	board[index(x, y)]->setNeighbours(neighbours);
}

void Minesweeper::reveal(int x, int y)
{
	int idx = index(x, y);
	if (board[idx]->isRevealed()) return;

	board[idx]->setRevealed();
	cd_win--;
	if (!board[idx]->isTrapped() && board[idx]->getNeighbours() == 0)
	{
		int xStart = (x == 0) ? x : x - 1;
		int xEnd = (x == size - 1) ? x : x + 1;

		int yStart = (y == 0) ? y : y - 1;
		int yEnd = (y == size - 1) ? y : y + 1;

		for (int i = yStart; i <= yEnd; i++)
		{
			for (int j = xStart; j <= xEnd; j++)
				if (i != y || j != x) reveal(j, i);
		}
	}
}

void Minesweeper::display()
{
	// first line
	std::cout << (char) 201;
	for (int i = 0; i < size; i++) std::cout << (char) 205;
	std::cout << (char) 187 << std::endl;

	// content
	for (int y = 0; y < size; y++)
	{
		std::cout << (char) 186;
		for (int x = 0; x < size; x++)
		{
			std::cout << board[index(x ,y)]->getDisplay();
		} // END for x
		std::cout << (char) 186 << std::endl;
	} // END for y

	// last line
	std::cout << (char) 200;
	for (int i = 0; i < size; i++) std::cout << (char) 205;
	std::cout << (char) 188 << std::endl;
}
