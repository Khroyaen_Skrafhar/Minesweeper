#ifndef CELL_H
#define CELL_H


class Cell
{
	public:
		/** Default constructor */
		Cell();
		Cell(bool trapped);

		/** Accessors */
		bool isTrapped() {return trapped;}
		bool isRevealed() {return revealed;}
		int getNeighbours() {return neighbours;}

		void setTrapped(bool trapped = true) {this->trapped = trapped;}
		void setRevealed(bool revealed = true) {this->revealed = revealed;}
		void setNeighbours(int neighbours) {this->neighbours = neighbours;}

		/** Functions */
		char getDisplay();

	protected:

	private:
		bool trapped;
		bool revealed;
		int neighbours;
};

#endif // CELL_H
