#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>

#include "cell.h"
#include "customutils.h"

class Minesweeper
{
	public:
		/** Default constructor */
		Minesweeper();
		/** Default destructor */
		virtual ~Minesweeper();

		/** Functions */
		void setup();
		void run();


	protected:

	private:
		Cell** board;
		int size;
		int cd_win;
		void updateNeighbours(int x, int y);
		void display();
		void reveal(int x, int y);
		size_t index(int x, int y) {return x + y * size;}

};

#endif // MINESWEEPER_H
